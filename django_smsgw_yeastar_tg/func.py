import urllib.parse

from asterisk.manager import ManagerSocketException, Manager
from django.conf import settings


def send_sms(number: str, message: str):
    manager = Manager()

    manager.connect(settings.YEASTAR_TG_SMS_API_HOST)
    manager.login(settings.YEASTAR_TG_SMS_API_USER, settings.YEASTAR_TG_SMS_API_PASSWD)

    message_urlencoded = urllib.parse.quote(message)

    try:
        manager.send_action(
            cdict={
                "Action": "smscommand",
                "command": "gsm send sms 2 "
                + number
                + ' "'
                + message_urlencoded
                + '" 1',
            }
        )
    except ManagerSocketException:
        manager.logoff()
        return False

    manager.logoff()

    return True
